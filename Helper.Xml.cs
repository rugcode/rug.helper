﻿using System;
using System.Globalization;
using System.Xml;
using System.Xml.Linq;

namespace Rug
{
    internal static partial class Helper
    {
        #region Node helpers

        public static void AppendAttributeAndValue(XElement element, string name, bool value)
        {
            AppendAttributeAndValue(element, name, value.ToString(CultureInfo.InvariantCulture));
        }

        public static void AppendAttributeAndValue(XElement element, string name, int value)
        {
            AppendAttributeAndValue(element, name, value.ToString(CultureInfo.InvariantCulture));
        }

        public static void AppendAttributeAndValue(XElement element, string name, uint value)
        {
            AppendAttributeAndValue(element, name, value.ToString(CultureInfo.InvariantCulture));
        }

        public static void AppendAttributeAndValue(XElement element, string name, short value)
        {
            AppendAttributeAndValue(element, name, value.ToString(CultureInfo.InvariantCulture));
        }

        public static void AppendAttributeAndValue(XElement element, string name, ushort value)
        {
            AppendAttributeAndValue(element, name, value.ToString(CultureInfo.InvariantCulture));
        }

        public static void AppendAttributeAndValue(XElement element, string name, float value)
        {
            AppendAttributeAndValue(element, name, value.ToString(CultureInfo.InvariantCulture));
        }

        public static void AppendAttributeAndValue(XElement element, string name, double value)
        {
            AppendAttributeAndValue(element, name, value.ToString(CultureInfo.InvariantCulture));
        }

        public static void AppendAttributeAndValue(XElement element, string name, decimal value)
        {
            AppendAttributeAndValue(element, name, value.ToString(CultureInfo.InvariantCulture));
        }

        public static void AppendAttributeAndValue<TResult>(XElement element, string name, TResult value) where TResult : struct, IConvertible
        {
            AppendAttributeAndValue(element, name, value.ToString());
        }

        public static void AppendAttributeAndValue(XElement element, string name, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return;
            }

            element.Add(new XAttribute(name, value));
        }

        public static XElement FindChild(string name, XElement node)
        {
            return node.Element(name); 
        }

        public static TResult GetAttributeValue<TResult>(XElement node, string name, TResult @default) where TResult : struct, IConvertible
        {
            if (typeof(TResult).IsEnum == false)
            {
                throw new NotSupportedException("TResult must be an enumeration type.");
            }

            if ((node.Attribute(name) != null))
            {
                try
                {
                    return (TResult)Enum.Parse(typeof(TResult), node.Attribute(name).Value, true);
                }
                catch
                {
                    return @default;
                }
            }
            else
            {
                return @default;
            }
        }

        public static string GetAttributeValue(XElement node, string name, string @default)
        {
            if ((node.Attribute(name) != null))
            {
                return node.Attribute(name).Value;
            }
            else
            {
                return @default;
            }
        }

        public static int GetAttributeValue(XElement node, string name, int @default)
        {
            int @return;

            if ((node.Attribute(name) != null) && int.TryParse(node.Attribute(name).Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out @return))
            {
                return @return;
            }
            else
            {
                return @default;
            }
        }

        public static uint GetAttributeValue(XElement node, string name, uint @default)
        {
            uint @return;

            if ((node.Attribute(name) != null) && uint.TryParse(node.Attribute(name).Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out @return))
            {
                return @return;
            }
            else
            {
                return @default;
            }
        }

        public static short GetAttributeValue(XElement node, string name, short @default)
        {
            short @return;

            if ((node.Attribute(name) != null) && short.TryParse(node.Attribute(name).Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out @return))
            {
                return @return;
            }
            else
            {
                return @default;
            }
        }

        public static ushort GetAttributeValue(XElement node, string name, ushort @default)
        {
            ushort @return;

            if ((node.Attribute(name) != null) && ushort.TryParse(node.Attribute(name).Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out @return))
            {
                return @return;
            }
            else
            {
                return @default;
            }
        }

        public static float GetAttributeValue(XElement node, string name, float @default)
        {
            float @return;

            if ((node.Attribute(name) != null) && float.TryParse(node.Attribute(name)?.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out @return))
            {
                return @return;
            }
            else
            {
                return @default;
            }
        }

        public static double GetAttributeValue(XElement node, string name, double @default)
        {
            double @return;

            if ((node.Attribute(name) != null) && double.TryParse(node.Attribute(name)?.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out @return))
            {
                return @return;
            }
            else
            {
                return @default;
            }
        }

        public static decimal GetAttributeValue(XElement node, string name, decimal @default)
        {
            decimal @return;

            if ((node.Attribute(name) != null) && decimal.TryParse(node.Attribute(name)?.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out @return))
            {
                return @return;
            }
            else
            {
                return @default;
            }
        }

        public static bool GetAttributeValue(XElement node, string name, bool @default)
        {
            bool @return;

            if ((node.Attribute(name) != null) && bool.TryParse(node.Attribute(name)?.Value, out @return))
            {
                return @return;
            }
            else
            {
                return @default;
            }
        }

        public static bool IsAttributeValueTrue(XElement node, string name, bool @default)
        {
            return (node.Attribute(name) != null) ? Helper.IsTrueValue(node.Attribute(name)?.Value) : @default;
        }

        public static bool TryGetAttributeValue(XElement node, string name, out string value)
        {
            if (node.Attribute(name) != null)
            {
                value = node.Attribute(name)?.Value;
                return true;
            }
            else
            {
                value = null;
                return false;
            }
        }

        #endregion Node helpers

        public static void AppendCData(XElement node, string tag, string content)
        {
            if (string.IsNullOrEmpty(content) == true)
            {
                return;
            }

            node.Add(new XElement(tag, new XCData(content)));
        }

        //public static XElement CreateElement(XElement node, string tag)
        //{
        //    return (node as XmlDocument ?? node.OwnerDocument).CreateElement(tag);
        //}

        public static bool IsTrueValue(string value)
        {
            if (string.IsNullOrEmpty(value) == true)
            {
                return false;
            }

            string val = value.Trim();

            bool result = false;
            if (bool.TryParse(val, out result) == true)
            {
                return result;
            }

            if ("yes".Equals(val, StringComparison.InvariantCultureIgnoreCase) == true)
            {
                return true;
            }

            if ("1".Equals(val) == true)
            {
                return true;
            }

            return false;
        }

        public static string ReadCData(XElement node, string tag)
        {
            if (string.IsNullOrEmpty(tag) == true)
            {
                return node.Value; 
            }

            return node.Element(tag)?.Value ?? string.Empty;
        }
    }
}