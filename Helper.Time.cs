﻿using System;

namespace Rug
{
    internal static partial class Helper
    {
        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation.
        /// </summary>
        /// <param name="timeSpan">Time span to be represented as a string in the format hours:minutes:seconds:milliseconds.</param>
        /// <returns>Time span represented as a string.</returns>
        public static string TimeSpanToString(TimeSpan timeSpan)
        {
            return string.Format("{0:D2}:{1:D2}:{2:D2}.{3:D3}",
                                 (int)Math.Floor(timeSpan.TotalHours),
                                 timeSpan.Minutes,
                                 timeSpan.Seconds,
                                 timeSpan.Milliseconds);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation.
        /// </summary>
        /// <param name="dateTime">Date and time to be represented as a string in the format year-month-date hour:minute:second.millisecond.</param>
        /// <param name="includeMilliseconds"></param>
        /// <returns>Date and time represented as a string.</returns>
        public static string DateTimeToString(DateTime dateTime, bool includeMilliseconds = false)
        {
            if (includeMilliseconds)
            {
                return dateTime.ToString("yyyy-MM-dd HH:mm:ss.fff");
            }
            else
            {
                return dateTime.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }
    }
}
