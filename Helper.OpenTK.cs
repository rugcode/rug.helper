﻿using System;
using System.Globalization;
using System.Xml;
using System.Xml.Linq;
using OpenTK;

namespace Rug
{
    internal static partial class Helper
    {
        public static void AppendAttributeAndValue(XElement element, string name, Vector2 value) { Helper.AppendAttributeAndValue(element, name, SerializeVector2(value)); }
        public static void AppendAttributeAndValue(XElement element, string name, Vector3 value) { Helper.AppendAttributeAndValue(element, name, SerializeVector3(value)); }
        public static void AppendAttributeAndValue(XElement element, string name, Vector4 value) { Helper.AppendAttributeAndValue(element, name, SerializeVector4(value)); }
        public static void AppendAttributeAndValue(XElement element, string name, Quaternion value) { Helper.AppendAttributeAndValue(element, name, SerializeQuaternion(value)); }

        public static Vector2 DeserializeVector2(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            float x, y;

            x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = float.Parse(pieces[1], CultureInfo.InvariantCulture);

            return new Vector2(x, y);
        }

        public static Vector3 DeserializeVector3(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            float x, y, z;

            x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = float.Parse(pieces[1], CultureInfo.InvariantCulture);
            z = float.Parse(pieces[2], CultureInfo.InvariantCulture);

            return new Vector3(x, y, z);
        }

        public static Vector4 DeserializeVector4(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            float x, y, z, w;

            x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = float.Parse(pieces[1], CultureInfo.InvariantCulture);
            z = float.Parse(pieces[2], CultureInfo.InvariantCulture);
            w = float.Parse(pieces[3], CultureInfo.InvariantCulture);

            return new Vector4(x, y, z, w);
        }

        public static Quaternion DeserializeQuaternion(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            float x, y, z, w;

            x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = float.Parse(pieces[1], CultureInfo.InvariantCulture);
            z = float.Parse(pieces[2], CultureInfo.InvariantCulture);
            w = float.Parse(pieces[3], CultureInfo.InvariantCulture);

            return new Quaternion(x, y, z, w);
        }
        public static Vector2 GetAttributeValue(XElement node, string name, Vector2 @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializeVector2(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }

        public static Vector3 GetAttributeValue(XElement node, string name, Vector3 @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializeVector3(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }

        public static Vector4 GetAttributeValue(XElement node, string name, Vector4 @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializeVector4(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }

        public static Quaternion GetAttributeValue(XElement node, string name, Quaternion @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializeQuaternion(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }
        

        public static string SerializeVector2(Vector2 point)
        {
            return $"{point.X.ToString(CultureInfo.InvariantCulture)},{point.Y.ToString(CultureInfo.InvariantCulture)}";
        }

        public static string SerializeVector3(Vector3 point)
        {
            return $"{point.X.ToString(CultureInfo.InvariantCulture)},{point.Y.ToString(CultureInfo.InvariantCulture)},{point.Z.ToString(CultureInfo.InvariantCulture)}";
        }

        public static string SerializeVector4(Vector4 point)
        {
            return string.Format("{0},{1},{2},{4}",
                point.X.ToString(CultureInfo.InvariantCulture),
                point.Y.ToString(CultureInfo.InvariantCulture),
                point.Z.ToString(CultureInfo.InvariantCulture),
                point.W.ToString(CultureInfo.InvariantCulture));
        }

        public static string SerializeQuaternion(Quaternion point)
        {
            return string.Format("{0},{1},{2},{4}",
                point.X.ToString(CultureInfo.InvariantCulture),
                point.Y.ToString(CultureInfo.InvariantCulture),
                point.Z.ToString(CultureInfo.InvariantCulture),
                point.W.ToString(CultureInfo.InvariantCulture));
        }        
    }
}