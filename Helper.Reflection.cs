﻿using System;
using System.Reflection;

namespace Rug
{
    internal static partial class Helper
    {
        public static TAttribute GetSingleAttribute<TAttribute>(MemberInfo memberInfo, bool inherit) where TAttribute : Attribute
        {
            TAttribute attribute;

            TryGetSingleAttribute(memberInfo, inherit, out attribute);

            return attribute;
        }

        public static TAttribute GetSingleAttribute<TAttribute>(Type type, bool inherit) where TAttribute : Attribute
        {
            TAttribute attribute;

            TryGetSingleAttribute(type, inherit, out attribute);

            return attribute;
        }

        public static bool HasAttribute<TAttribute>(MemberInfo memberInfo, bool inherit) where TAttribute : Attribute
        {
            object[] attributes;

            if (inherit == true)
            {
                attributes = Attribute.GetCustomAttributes(memberInfo, typeof(TAttribute), true);
            }
            else
            {
                attributes = memberInfo.GetCustomAttributes(typeof(TAttribute), false);
            }

            return attributes.Length > 0;
        }

        public static bool HasAttribute<TAttribute>(Type type, bool inherit) where TAttribute : Attribute
        {
            object[] attributes = type.GetCustomAttributes(typeof(TAttribute), inherit);

            return attributes.Length > 0;
        }

        public static bool ImplementsInterface(Type type, Type interfaceType)
        {
            return type.GetInterface(interfaceType.FullName) != null;
        }

        public static bool TryGetSingleAttribute<TAttribute>(MemberInfo memberInfo, bool inherit, out TAttribute attribute) where TAttribute : Attribute
        {
            object[] attributes;

            if (inherit == true)
            {
                attributes = Attribute.GetCustomAttributes(memberInfo, typeof(TAttribute), true);
            }
            else
            {
                attributes = memberInfo.GetCustomAttributes(typeof(TAttribute), false);
            }

            if (attributes.Length == 0)
            {
                attribute = null;
                return false;
            }

            attribute = (TAttribute)attributes[0];
            return true;
        }

        public static bool TryGetSingleAttribute<TAttribute>(Type type, bool inherit, out TAttribute attribute) where TAttribute : Attribute
        {
            object[] attributes = type.GetCustomAttributes(typeof(TAttribute), inherit);

            if (attributes.Length == 0)
            {
                attribute = null;
                return false;
            }

            attribute = (TAttribute)attributes[0];
            return true;
        }
    }
}