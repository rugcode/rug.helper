﻿using System;
using System.Globalization;
using System.Xml;
using System.Xml.Linq;
using OscNode;

namespace Rug
{
    internal static partial class Helper
    {
        public static void AppendAttributeAndValue(XElement element, string name, Bounds2D value) { Rug.Helper.AppendAttributeAndValue(element, name, SerializeBounds2D(value)); }

		public static Bounds2D GetAttributeValue(XElement node, string name, Bounds2D @default)
		{
			if (node.Attribute(name) == null)
			{
				return @default;
			}

			try
			{
				return DeserializeBounds2D(node.Attribute(name).Value);
			}
			catch
			{
				return @default;
			}
		}

        #region Bounds Helpers

        public static string SerializeBounds2D(Bounds2D rect)
		{
			return string.Format("{0},{1},{2},{3}",
				rect.X.ToString(CultureInfo.InvariantCulture),
				rect.Y.ToString(CultureInfo.InvariantCulture),
				rect.Width.ToString(CultureInfo.InvariantCulture),
				rect.Height.ToString(CultureInfo.InvariantCulture));
		}

		public static Bounds2D DeserializeBounds2D(string str)
		{
			string[] pieces = str.Split(new char[] { ',' });
			float x, y, width, height;

			x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
			y = float.Parse(pieces[1], CultureInfo.InvariantCulture);
			width = float.Parse(pieces[2], CultureInfo.InvariantCulture);
			height = float.Parse(pieces[3], CultureInfo.InvariantCulture);

			return new Bounds2D(x, y, width, height);
		}

        #endregion
	}
}