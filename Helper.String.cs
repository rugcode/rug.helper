﻿
using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Rug
{
    internal static partial class Helper
	{				
		#region String Helpers

		public static bool IsNullOrEmpty(string str)
		{
			if (str == null)
			{
				return true;
			}

            if (str == string.Empty)
            {
                return true;
            }

            if (str.Trim().Length == 0)
			{
				return true;
			}

			return false;
		}
        
        private static Regex AlphaNumericFilter = new Regex("[^A-Z0-9]+", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public static string ToLowerCamelCase(string original)
        {
            string str = AlphaNumericFilter.Replace(original, " ");

            StringBuilder sb = new StringBuilder(str.Length);

            bool first = true; 

            foreach (string part in str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (first == true)
                {
                    first = false; 
                    sb.Append(part.ToLowerInvariant());
                }
                else
                {
                    sb.Append(char.ToUpperInvariant(part[0]));
                    sb.Append(part.Substring(1)); 
                }
            }

            return sb.ToString(); 
        }

        public static string ToLowerSplitWithHyphen(string original)
        {
            StringBuilder sb = new StringBuilder(original.Length);

            for (int i = 0; i < original.Length; i++) 
            {
                if (char.IsUpper(original, i) == true)
                {
                    if (i > 0)
                    {
                        sb.Append("-"); 
                    }
                }

                sb.Append(char.ToLowerInvariant(original[i]));                    
            }

            return sb.ToString();
        }

        public static bool IsInvalidFileName(string filename)
        {
            return filename.IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) >= 0;
        }

        #endregion
    }
}
