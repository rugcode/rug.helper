﻿using System;
using System.Drawing;
using System.Globalization;
using System.Xml;
using System.Xml.Linq;

namespace Rug
{
    internal static partial class Helper
    {
        public static void AppendAttributeAndValue(XElement element, string name, Rectangle value) { Helper.AppendAttributeAndValue(element, name, SerializeRectangle(value)); }
        public static void AppendAttributeAndValue(XElement element, string name, RectangleF value) { Helper.AppendAttributeAndValue(element, name, SerializeRectangleF(value)); }

        public static void AppendAttributeAndValue(XElement element, string name, PointF value) { Helper.AppendAttributeAndValue(element, name, SerializePointF(value)); }
        public static void AppendAttributeAndValue(XElement element, string name, Point value) { Helper.AppendAttributeAndValue(element, name, SerializePoint(value)); }

        public static void AppendAttributeAndValue(XElement element, string name, Size value) { Helper.AppendAttributeAndValue(element, name, SerializeSize(value)); }
        public static void AppendAttributeAndValue(XElement element, string name, SizeF value) { Helper.AppendAttributeAndValue(element, name, SerializeSizeF(value)); }

        public static Rectangle GetAttributeValue(XElement node, string name, Rectangle @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializeRectangle(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }

        public static RectangleF GetAttributeValue(XElement node, string name, RectangleF @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializeRectangleF(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }


        public static Size GetAttributeValue(XElement node, string name, Size @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializeSize(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }

        public static SizeF GetAttributeValue(XElement node, string name, SizeF @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializeSizeF(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }

        public static Point GetAttributeValue(XElement node, string name, Point @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializePoint(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }

        public static PointF GetAttributeValue(XElement node, string name, PointF @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializePointF(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }

        #region Rectangle Helpers

        public static string SerializeRectangle(Rectangle rect)
        {
            return string.Format("{0},{1},{2},{3}",
                rect.X.ToString(CultureInfo.InvariantCulture),
                rect.Y.ToString(CultureInfo.InvariantCulture),
                rect.Width.ToString(CultureInfo.InvariantCulture),
                rect.Height.ToString(CultureInfo.InvariantCulture));
        }

        public static Rectangle DeserializeRectangle(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            int x, y, width, height;

            x = int.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = int.Parse(pieces[1], CultureInfo.InvariantCulture);
            width = int.Parse(pieces[2], CultureInfo.InvariantCulture);
            height = int.Parse(pieces[3], CultureInfo.InvariantCulture);

            return new Rectangle(x, y, width, height);
        }

        public static string SerializeRectangleF(System.Drawing.RectangleF rect)
        {
            return string.Format("{0},{1},{2},{3}",
                rect.X.ToString(CultureInfo.InvariantCulture),
                rect.Y.ToString(CultureInfo.InvariantCulture),
                rect.Width.ToString(CultureInfo.InvariantCulture),
                rect.Height.ToString(CultureInfo.InvariantCulture));
        }

        public static System.Drawing.RectangleF DeserializeRectangleF(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            float x, y, width, height;

            x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = float.Parse(pieces[1], CultureInfo.InvariantCulture);
            width = float.Parse(pieces[2], CultureInfo.InvariantCulture);
            height = float.Parse(pieces[3], CultureInfo.InvariantCulture);

            return new System.Drawing.RectangleF(x, y, width, height);
        }

        #endregion

        #region Point Helpers

        public static string SerializePoint(Point point)
        {
            return $"{point.X.ToString(CultureInfo.InvariantCulture)},{point.Y.ToString(CultureInfo.InvariantCulture)}";
        }

        public static Point DeserializePoint(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            int x, y;

            x = int.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = int.Parse(pieces[1], CultureInfo.InvariantCulture);

            return new Point(x, y);
        }

        public static string SerializePointF(PointF point)
        {
            return $"{point.X.ToString(CultureInfo.InvariantCulture)},{point.Y.ToString(CultureInfo.InvariantCulture)}";
        }

        public static PointF DeserializePointF(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            float x, y;

            x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = float.Parse(pieces[1], CultureInfo.InvariantCulture);

            return new PointF(x, y);
        }

        #endregion

        #region Size Helpers

        public static string SerializeSize(Size size)
        {
            return $"{size.Width.ToString(CultureInfo.InvariantCulture)},{size.Height.ToString(CultureInfo.InvariantCulture)}";
        }

        public static Size DeserializeSize(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            int x, y;

            x = int.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = int.Parse(pieces[1], CultureInfo.InvariantCulture);

            return new Size(x, y);
        }

        public static string SerializeSizeF(SizeF size)
        {
            return $"{size.Width.ToString(CultureInfo.InvariantCulture)},{size.Height.ToString(CultureInfo.InvariantCulture)}";
        }

        public static SizeF DeserializeSizeF(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            float x, y;

            x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = float.Parse(pieces[1], CultureInfo.InvariantCulture);

            return new SizeF(x, y);
        }

        #endregion
    }
}