﻿using OscNode.Rgb;
using System.Globalization;
using System.Xml;
using System.Xml.Linq;

namespace Rug
{
    internal static partial class Helper
    {
        public static void AppendAttributeAndValue(XElement element, string name, ColorRgb value)
        {
            Helper.AppendAttributeAndValue(element, name, SerializeColorRgb(value));
        }

        public static void AppendAttributeAndValue(XElement element, string name, ColorRgbF value)
        {
            Helper.AppendAttributeAndValue(element, name, SerializeColorRgbF(value));
        }

        public static void AppendAttributeAndValue(XElement element, string name, ColorRgba value)
        {
            Helper.AppendAttributeAndValue(element, name, SerializeColorRgba(value));
        }

        public static void AppendAttributeAndValue(XElement element, string name, ColorRgbaF value)
        {
            Helper.AppendAttributeAndValue(element, name, SerializeColorRgbaF(value));
        }

        public static ColorRgb DeserializeColorRgb(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            byte r, g, b;

            r = byte.Parse(pieces[0], CultureInfo.InvariantCulture);
            g = byte.Parse(pieces[1], CultureInfo.InvariantCulture);
            b = byte.Parse(pieces[2], CultureInfo.InvariantCulture);

            return new ColorRgb(r, g, b);
        }

        public static ColorRgba DeserializeColorRgba(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            byte r, g, b, a;

            r = byte.Parse(pieces[0], CultureInfo.InvariantCulture);
            g = byte.Parse(pieces[1], CultureInfo.InvariantCulture);
            b = byte.Parse(pieces[2], CultureInfo.InvariantCulture);
            a = byte.Parse(pieces[3], CultureInfo.InvariantCulture);

            return new ColorRgba(r, g, b, a);
        }

        public static ColorRgbaF DeserializeColorRgbaF(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            float r, g, b, a;

            r = float.Parse(pieces[0], CultureInfo.InvariantCulture);
            g = float.Parse(pieces[1], CultureInfo.InvariantCulture);
            b = float.Parse(pieces[2], CultureInfo.InvariantCulture);
            a = float.Parse(pieces[3], CultureInfo.InvariantCulture);

            return new ColorRgbaF(r, g, b, a);
        }

        public static ColorRgbF DeserializeColorRgbF(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            float r, g, b;

            r = float.Parse(pieces[0], CultureInfo.InvariantCulture);
            g = float.Parse(pieces[1], CultureInfo.InvariantCulture);
            b = float.Parse(pieces[2], CultureInfo.InvariantCulture);

            return new ColorRgbF(r, g, b);
        }

        public static ColorRgb GetAttributeValue(XElement node, string name, ColorRgb @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializeColorRgb(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }

        public static ColorRgbF GetAttributeValue(XElement node, string name, ColorRgbF @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializeColorRgbF(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }

        public static ColorRgba GetAttributeValue(XElement node, string name, ColorRgba @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializeColorRgba(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }

        public static ColorRgbaF GetAttributeValue(XElement node, string name, ColorRgbaF @default)
        {
            if (node.Attribute(name) == null)
            {
                return @default;
            }

            try
            {
                return DeserializeColorRgbaF(node.Attribute(name).Value);
            }
            catch
            {
                return @default;
            }
        }

        public static string SerializeColorRgb(ColorRgb color)
        {
            return $"{color.R.ToString(CultureInfo.InvariantCulture)},{color.G.ToString(CultureInfo.InvariantCulture)},{color.B.ToString(CultureInfo.InvariantCulture)}";
        }

        public static string SerializeColorRgba(ColorRgba color)
        {
            return $"{color.R.ToString(CultureInfo.InvariantCulture)},{color.G.ToString(CultureInfo.InvariantCulture)},{color.B.ToString(CultureInfo.InvariantCulture)},{color.A.ToString(CultureInfo.InvariantCulture)}";
        }

        public static string SerializeColorRgbaF(ColorRgbaF color)
        {
            return $"{color.R.ToString(CultureInfo.InvariantCulture)},{color.G.ToString(CultureInfo.InvariantCulture)},{color.B.ToString(CultureInfo.InvariantCulture)},{color.A.ToString(CultureInfo.InvariantCulture)}";
        }

        public static string SerializeColorRgbF(ColorRgbF color)
        {
            return $"{color.R.ToString(CultureInfo.InvariantCulture)},{color.G.ToString(CultureInfo.InvariantCulture)},{color.B.ToString(CultureInfo.InvariantCulture)}";
        }
    }
}